﻿using System;
using System.Collections.Generic;

namespace PruebaTecnica.EnumExtensions
{
    /// <summary>
    /// Class Extending Integer functionallity
    /// </summary>
    public static class EnumExtensions
    {
        
        /// <summary>
        /// Returns next enum value, if the current value is the last value, it returns that value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <returns></returns>
        public static T Next<T>(this T src) where T : Enum
        {
            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Math.Min(Arr.Length - 1, Array.IndexOf<T>(Arr, src) + 1);
            return Arr[j];
        }

        /// <summary>
        /// Returns previous enum value, if the current value is the first value, it returns that value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <returns></returns>
        public static T Previous<T>(this T src) where T : Enum
        {
            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Math.Max(0, Array.IndexOf<T>(Arr, src) - 1);
            return Arr[j];
        }
    }
}
