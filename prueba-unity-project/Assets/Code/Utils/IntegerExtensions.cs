﻿using System;
using System.Collections.Generic;

namespace PruebaTecnica.IntegerExtensions
{
    /// <summary>
    /// Class Extending Integer functionallity
    /// </summary>
    public static class IntegerExtensions
    {   
        /// <summary>
        /// Struct that allow us to store the noum of a number and the possible variation for combinations
        /// </summary>
        public struct NumberName
        {
            public string Noun;
            public string Combination;

            public NumberName(string noun, string combination = "")
            {
                this.Noun = noun;
                this.Combination = string.IsNullOrEmpty(combination) ? noun : combination;
            }
        }

        /// <summary>
        /// All the different Spanish noums and combination variation we need to compound number names
        /// </summary>
        private static Dictionary<int, NumberName> NumberNamesSpanishMap = new Dictionary<int, NumberName> {
            { 0 ,   new NumberName("cero")                      },
            { 1 ,   new NumberName("uno")                       },
            { 2 ,   new NumberName("dos")                       },
            { 3 ,   new NumberName("tres")                      },
            { 4 ,   new NumberName("cuatro")                    },
            { 5 ,   new NumberName("cinco")                     },
            { 6 ,   new NumberName("seis")                      },
            { 7 ,   new NumberName("siete")                     },
            { 8 ,   new NumberName("ocho")                      },
            { 9 ,   new NumberName("nueve")                     },
            { 10 ,  new NumberName("diez","dieci")              },
            { 11 ,  new NumberName("once")                      },
            { 12 ,  new NumberName("doce")                      },
            { 13 ,  new NumberName("trece")                     },
            { 14 ,  new NumberName("catorce")                   },
            { 15 ,  new NumberName("quince")                    },
            { 20 ,  new NumberName("veinte",    "veinti")       },
            { 30 ,  new NumberName("treinta",   "treinta y ")   },
            { 40 ,  new NumberName("cuarenta",  "cuarenta y ")  },
            { 50 ,  new NumberName("cincuenta", "cincuenta y ") },
            { 60 ,  new NumberName("sesenta",   "sesenta y ")   },
            { 70 ,  new NumberName("setenta",   "setenta y ")    },
            { 80 ,  new NumberName("ochenta",   "ochenta y ")   },
            { 90 ,  new NumberName("noventa",   "noventa y ")   },
            { 100 , new NumberName("cien",      "ciento ")      },
            { 200 , new NumberName("doscientos ")                },
            { 300 , new NumberName("trescientos ")               },
            { 400 , new NumberName("cuatrocientos ")             },
            { 500 , new NumberName("quinientos ")                },
            { 600 , new NumberName("seiscientos ")               },
            { 700 , new NumberName("setecientos ")               },
            { 800 , new NumberName("ochocientos ")               },
            { 900 , new NumberName("novecientos ")               },
            { 1000, new NumberName("mil ")                       }

        };

        
        /// <summary>
        /// Integer Extension method, (e.g. , mynumber.ToWords()) 
        /// </summary>
        /// <param name="variable">Not even asked, the compiler passes the own variable as a parameter</param>
        /// <returns></returns>
        public static string ToWords(this int variable)
        {
            // calling the numbertowords method with a deep of 10 that should be enough for numbers we are expecting.
            return NumberToWords(variable, 10);
        }

        /// <summary>
        /// Recursive Method that allows us to compound the number name into words, called by the extension
        /// </summary>
        /// <param name="number"></param>
        /// <param name="deep"></param>
        /// <returns></returns>
        private static string NumberToWords(int number, int deep)
        {
            // Just as a warrantly it will not cause an StackOverflow
            if (deep-- <= 0) return "";

            // if the number is in the map of names we return it inmediatly
            if (NumberNamesSpanishMap.ContainsKey(number)) return NumberNamesSpanishMap[number].Noun;

            // if the number is negative we add the "menos" statement
            if (number < 0) return "menos " + NumberToWords(Math.Abs(number), deep);


            string result = "";

            // auxiliar variables that allow us not to do the same operation a lot of times
            int aux = 0;
            int numberdividedbythousand = number / 1000;
            int numberdividedbyhundred = number / 100;
            int numberdividedbyten = number / 10;

            // It can be one of three cases
            //      * number > 1000 -> in this case the result will be Recursively(number / 1000) + "mil" +  Recursively(number % 1000)
            //      * number > 100 -> in this case the result will be the hundred combination variation + Recursively( number % 100) 
            //      * number > 10 -> in this case the result will be the ten times combination variation + Recursively( number %10) 

            if (numberdividedbythousand > 0)
            {
                result += NumberToWords(numberdividedbythousand, deep) + " " +NumberNamesSpanishMap[1000].Noun  + NumberToWords(number % 1000, deep);
            }
            else if (numberdividedbyhundred > 0)
            {
                aux = numberdividedbyhundred * 100;

                if (NumberNamesSpanishMap.ContainsKey(aux))
                { 
                    result += NumberNamesSpanishMap[aux].Combination + NumberToWords(number % 100, deep);
                }
            }
            else if (numberdividedbyten > 0)
            {
                aux = numberdividedbyten * 10;

                if (NumberNamesSpanishMap.ContainsKey(aux))
                {
                    result += NumberNamesSpanishMap[aux].Combination + NumberToWords(number % 10, deep);
                }
            }

            return result;
        }

    }
}
