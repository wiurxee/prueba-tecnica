﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PruebaTecnica.AssetsManagement
{
    public static class AssetLoader
    {
        public static T LoadAssetSync<T>(string assetpath) where T : Object
        {
            Debug.Log("[Asset Loader] : loading : " + assetpath);
            return Resources.Load<T>(assetpath);
        }
        public static T InstantiateAssetSync<T>(string assetpath, Transform parent = null, bool worldPositionStays = false) where T : Object
        {
            return Object.Instantiate<T>(LoadAssetSync<T>(assetpath), parent, worldPositionStays);
        }
        public static T InstantiateAssetSync<T>(string assetpath, Vector3 position, Quaternion rotation, Transform parent = null) where T : Object
        {
            return Object.Instantiate<T>(LoadAssetSync<T>(assetpath), position, rotation, parent);
        }
    }
}
