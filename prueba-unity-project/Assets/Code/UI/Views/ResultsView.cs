﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class ResultsView : View
    {
        protected override ShowPanelType ShowType => ShowPanelType.FadeIn;

        [SerializeField]
        private ExtendedText m_SuccesesText;
        [SerializeField]
        private ExtendedText m_FailuresText;
        [SerializeField]
        private ExtendedText m_DifficultyText;


        public void SetSuccesses(int succeses)
        {
            m_SuccesesText.text = succeses.ToString();
        }

        public void SetFailures(int failures)
        {
            m_FailuresText.text = failures.ToString();
        }

        public void SetDifficulty(string difficulty)
        {
            m_DifficultyText.text = difficulty;
        }
    }
}
