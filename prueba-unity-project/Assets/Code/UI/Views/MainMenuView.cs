﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class MainMenuView : View
    {
        protected override ShowPanelType ShowType => ShowPanelType.FadeIn;

        [SerializeField]
        private Text m_WelcomeText;

        [SerializeField]
        private Button m_StartButton;

        public void SetWelcomeText(string text)
        {
            m_WelcomeText.text = text;
        }

        public void SubscribeStartButton(UnityAction callback)
        {
            m_StartButton.onClick.AddListener(callback);
        }

        public void UnsubscribeStartButton(UnityAction callback)
        {
            m_StartButton.onClick.RemoveListener(callback);
        }
    }
}
