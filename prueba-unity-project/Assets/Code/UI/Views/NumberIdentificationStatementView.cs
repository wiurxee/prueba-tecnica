﻿using PruebaTecnica.IntegerExtensions;
using UnityEngine;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class NumberIdentificationStatementView : View
    {
        protected override ShowPanelType ShowType => ShowPanelType.FadeIn;

        [SerializeField]
        private Text m_tittle;
        [SerializeField]
        private Text m_Statement;

        public void SetTittle(string text)
        {
            m_tittle.text = text;
        }
        public void SetNumber(int number)
        {
            m_Statement.text = number.ToWords();            
        }
    }
}
