﻿using PruebaTecnica.AssetsManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class NumberIdentificationAnswersView : View
    {
        protected override ShowPanelType ShowType => ShowPanelType.MoveFromBelow;

        private List<AnswerButton> m_Answers = new List<AnswerButton>();

        [SerializeField]
        private Text m_Tittle;

        [SerializeField]
        private Transform m_AnswersContainer;

        [SerializeField]
        private AnswerButton m_AnswerPrefab;


        private Action<int> m_ButtonsCallbacks;

        public void SetTittle(string text)
        {
            if (m_Tittle != null) m_Tittle.text = text;
        }
        public void SetAnswers(List<int> answers)
        {
            foreach (AnswerButton answer in m_Answers)
            {
                answer.UnsubscribeClickEvent(OnButtonClicked);
                Destroy(answer.gameObject);
            }

            m_Answers.Clear();

            foreach (int answer in answers)
            {
                AnswerButton a = AssetLoader.InstantiateAssetSync<AnswerButton>("UI/Items/AnswerButton", m_AnswersContainer, false);
                a.AnswerNumber = answer;
                a.SubscribeClickEvent(OnButtonClicked);

                m_Answers.Add(a);
            }
        }
        public void SubscribeAnswersClicks(Action<int> callback)
        {
            m_ButtonsCallbacks += callback;
        }

        public void UnsubscribeAnswersClicks(Action<int> callback)
        {
            m_ButtonsCallbacks -= callback;
        }

        private void OnButtonClicked(int value)
        {
            m_ButtonsCallbacks?.Invoke(value);
        }


        public IEnumerator ChangeColorOverTime(int answerValue, Color color, float time, bool deactivatebutton = false)
        {
            foreach (AnswerButton answer in m_Answers)
            {
                if (answer.AnswerNumber == answerValue)
                {
                    yield return StartCoroutine(answer.ChangeColor(color, time));
                    if (deactivatebutton) answer.interactable = false;
                }
            }
        }
    }
}
