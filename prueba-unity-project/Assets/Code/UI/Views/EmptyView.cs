﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class EmptyView : View
    {
        protected override ShowPanelType ShowType => ShowPanelType.None;
    }
}
