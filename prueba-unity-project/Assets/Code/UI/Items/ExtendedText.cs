﻿using UnityEngine.UI;

namespace PruebaTecnica.UI
{
    public class ExtendedText : Text
    {
        public string Format;

        public override string text 
        { 
            get => base.text; 
            set
            { 
                base.text = string.Format(Format, value); 
            } 
        }
    }
}
