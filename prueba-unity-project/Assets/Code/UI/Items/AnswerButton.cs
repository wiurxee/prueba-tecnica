﻿

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PruebaTecnica.UI
{ 
    public class AnswerButton : Button
    {
        public Text InnerText;

        public Image BackGroundImage;

        private int m_AnswerNumber;
        public int AnswerNumber
        {
            get { return m_AnswerNumber; }
            set
            {
                m_AnswerNumber = value;

                if (InnerText != null)
                {
                    InnerText.text = m_AnswerNumber.ToString();
                }
            }
        }

        private Action<int> ClickEvent;
        public void SubscribeClickEvent(Action<int> callback)
        {
            ClickEvent += callback;
            onClick.AddListener(ButtonClicked);
        }

        public void UnsubscribeClickEvent(Action<int> callback)
        {
            ClickEvent -= callback;
            onClick.RemoveListener(ButtonClicked);
        }

        private void ButtonClicked()
        {
            ClickEvent?.Invoke(AnswerNumber);
        }


        public IEnumerator ChangeColor(Color targetColor, float time)
        {
            if (BackGroundImage != null)
            {
                float step = 0;
                Color fromColor = BackGroundImage.color;

                while (step < 1)
                {
                    step += Time.deltaTime / time;

                    BackGroundImage.color = Color.Lerp(fromColor, targetColor, step);

                    yield return new WaitForEndOfFrame();
                }

                BackGroundImage.color = targetColor;
            }
        }
    }
}
