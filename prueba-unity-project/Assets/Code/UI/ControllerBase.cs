﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PruebaTecnica.UI
{
    public abstract class ControllerBase : MonoBehaviour
    {
        [HideInInspector]
        public bool Modal = false;
        [HideInInspector]
        public bool ClosingAllowed = true;
        [HideInInspector]
        public bool IsOpened = false;

        protected ControllerBase m_Parent;
        protected List<ControllerBase> m_Childs;
        public abstract IEnumerator Show(bool modal);
        public abstract IEnumerator Hide();
        public abstract void Destroy();
        public abstract void SubscribeToEvents();

        public abstract void UnsubscribeFromEvents();

        public abstract void SetInteractable(bool value);
        private void CloseChildren()
        {
            // primero cerramos los hijos...
            if (m_Childs != null)
            {
                foreach (ControllerBase child in m_Childs)
                {
                    if (child.IsOpened)
                        child.Hide();
                }
            }
        }
        public IEnumerator CloseAllExcept(List<ControllerBase> exceptions)
        {
            if (m_Childs != null)
            {

                for (int i = 0; i < m_Childs.Count; i++)
                {
                    if(m_Childs[i].IsOpened) yield return StartCoroutine(m_Childs[i].CloseAllExcept(exceptions));
                }
            }

            if (exceptions != null && exceptions.Contains(this))
                yield break;

            if (!Modal && ClosingAllowed) yield return StartCoroutine(UIManager.Instance.HideScreen(this));
        }
        public List<ControllerBase> GetChildrenPanels()
        {
            return m_Childs;
        }
        public List<ControllerBase> GetHierarchy()
        {
            List<ControllerBase> result = new List<ControllerBase>();
            if (m_Parent != null)
            {
                result = m_Parent.GetHierarchy();
            }

            result.Add(this);

            return result;
        }

        private void SetParent(ControllerBase parent)
        {
            if (m_Parent != null && m_Parent != parent)
                m_Parent.RemoveChild(this);

            m_Parent = parent;
        }
        public void AddChild(ControllerBase child)
        {
            if (child == null) return;

            if (m_Childs == null)
                m_Childs = new List<ControllerBase>();

            if (!m_Childs.Contains(child))
                m_Childs.Add(child);

            child.SetParent(this);
        }

        //---------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------
        public void RemoveChild(ControllerBase child)
        {
            if (m_Childs != null && m_Childs.Contains(child))
                m_Childs.Remove(child);
        }
    }
}
