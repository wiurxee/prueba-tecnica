﻿using UnityEngine;
using PruebaTecnica.IntegerExtensions;
using System.Collections;
using UnityEngine.Events;

namespace PruebaTecnica.UI
{
    public abstract class View: ViewBase
    {
        public enum ShowPanelType
        {
            None,
            FadeIn,
            MoveFromBelow
        }
        protected abstract ShowPanelType ShowType { get; }

        private WaitForEndOfFrame m_Waiter;
        private CanvasGroup m_CanvasGroup;
        public override void Initialize()
        {
            m_Waiter = new WaitForEndOfFrame();
            m_CanvasGroup = gameObject.AddComponent<CanvasGroup>();

            m_CanvasGroup.alpha = 0;
        }
        public override void Destroy()
        { 
            
        }
        public override IEnumerator Show()
        {
            m_CanvasGroup.interactable = false;

            switch (ShowType)
            {
                case ShowPanelType.None:
                    m_CanvasGroup.alpha = 1;
                    break;
                case ShowPanelType.FadeIn:
                    yield return StartCoroutine(Fade(0f,1f, 2f));
                    break;
                case ShowPanelType.MoveFromBelow:
                    Vector2 initialPos = new Vector2(0, -Screen.height);
                    transform.position = initialPos;
                    Vector2 finalPos = Vector2.zero;
                    m_CanvasGroup.alpha = 1;
                    yield return StartCoroutine(Move(initialPos, finalPos, 2f));
                    break;
            }

            m_CanvasGroup.interactable = true;
        }

        public override IEnumerator Hide()
        {
            m_CanvasGroup.interactable = false;

            switch (ShowType)
            {
                case ShowPanelType.None:
                    m_CanvasGroup.alpha = 0;
                    break;
                case ShowPanelType.FadeIn:
                    yield return StartCoroutine(Fade(1f,0f, 2f));
                    break;
                case ShowPanelType.MoveFromBelow:
                    Vector2 initialPos = new Vector2(0, -Screen.height);
                    transform.position = initialPos;
                    Vector2 finalPos = Vector2.zero;

                    yield return StartCoroutine(Move(finalPos, initialPos, 2f));
                    break;
            }

            m_CanvasGroup.alpha = 0;
        }

        public override void SetInteractable(bool value)
        {
            if (m_CanvasGroup != null)
            {
                m_CanvasGroup.interactable = value;
            }
        }
        IEnumerator Fade(float from, float targetAlpha, float animationDuration)
        {
            m_CanvasGroup.alpha = from;

            if (m_CanvasGroup != null)
            {
                float step = 0;
                float fromAlpha = m_CanvasGroup.alpha;

                while (step < 1)
                {
                    step += Time.deltaTime / animationDuration;
                    m_CanvasGroup.alpha = Mathf.Lerp(fromAlpha, targetAlpha, step);

                    yield return m_Waiter;
                }

                m_CanvasGroup.alpha = targetAlpha;
            }
        }        
        IEnumerator Move(Vector2 from, Vector2 to, float animationDuration, UnityAction onEndAnimation = null)
        {
            float step = 0;

            Vector2 pos = transform.localPosition;

            while (step < 1)
            {
                step += Time.deltaTime / animationDuration;

                pos.x = Mathf.Lerp(from.x, to.x, step);
                pos.y = Mathf.Lerp(from.y, to.y, step);

                transform.localPosition = pos;

                yield return m_Waiter;
            }

            transform.localPosition = to;
            onEndAnimation?.Invoke();
        }
    }
}
