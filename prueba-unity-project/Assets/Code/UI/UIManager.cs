﻿using UnityEngine;
using System.Collections.Generic;
using System;
using PruebaTecnica.IntegerExtensions;
using PruebaTecnica.AssetsManagement;
using System.Collections;

namespace PruebaTecnica.UI
{
    public class UIManager : MonoBehaviour
    {

        public static UIManager Instance;

        public Canvas canvas;

        public ControllerBase RootPanel;
        [HideInInspector]
        public EmptyController MainPanel;
        [HideInInspector]
        public EmptyController ModalPanel;
        [HideInInspector]
        public EmptyController AlwaysOnTop;


        private Dictionary<string, ControllerBase> m_ScreenPool;

        private bool HasModalActive = false;
        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                DontDestroyOnLoad(this.gameObject);

                Initialize();
            }
            else
            {
                Destroy(this);
            }
        }

        public void Initialize()
        {
            m_ScreenPool = new Dictionary<string, ControllerBase>();

            MainPanel = (EmptyController) CreateScreen("main-panel");            
            ModalPanel = (EmptyController) CreateScreen("modal-panel");
            AlwaysOnTop = (EmptyController) CreateScreen("always-on-top-panel");

            MainPanel.InitializeController(new EmptyModel());
            ModalPanel.InitializeController(new EmptyModel());
            AlwaysOnTop.InitializeController(new EmptyModel());

            MainPanel.ClosingAllowed = false;
            ModalPanel.ClosingAllowed = false;
            AlwaysOnTop.ClosingAllowed = false;

            ShowScreen(MainPanel);
            ShowScreen(ModalPanel);
            ShowScreen(AlwaysOnTop);            
        }


        public ControllerBase CreateScreen(string screenId)
        {
            if(!m_ScreenPool.ContainsKey(screenId))
            {
                string path = "UI/Screens/" + screenId;
                ControllerBase screen = AssetLoader.InstantiateAssetSync<ControllerBase>(path, canvas.transform, false);
                m_ScreenPool[screenId] = screen;
            }

            return m_ScreenPool[screenId];
        }

        public void ShowScreen(ControllerBase screen, ControllerBase parent = null, bool modal = false, bool? closeOthers = null, bool firstSibling = false)
        {
            StartCoroutine(ShowScreenCoroutine(screen, parent, modal, closeOthers, firstSibling));
        }

        private IEnumerator ShowScreenCoroutine(ControllerBase screen, ControllerBase parent, bool modal, bool? closeOthers , bool firstSibling)
        {
            Debug.Log("[UIManager] : show screen  : " + screen.gameObject.name);

            bool closeBehindPanels = !modal && RootPanel != null;

            if (closeOthers.HasValue)
            {
                closeBehindPanels = closeOthers.Value;
            }

            if (modal)
            {
                closeBehindPanels = false;
                parent = ModalPanel;
            }

            HasModalActive = modal;

            if (parent == null) parent = RootPanel;

            screen.transform.SetParent(parent.transform, false);
            if (firstSibling) screen.transform.SetAsFirstSibling();
            else screen.transform.SetAsLastSibling();

            parent.AddChild(screen);

            if (modal && MainPanel != null) MainPanel.SetInteractable(false);            

            List<ControllerBase> hierarchyScreens = screen.GetHierarchy();
            if (closeBehindPanels) yield return StartCoroutine(RootPanel.CloseAllExcept(hierarchyScreens));

            screen.gameObject.SetActive(true);
            yield return StartCoroutine(screen.Show(modal));
            screen.SubscribeToEvents();
        }
        public IEnumerator HideScreen(ControllerBase screen)
        {
            Debug.Log("[UIManager] : hide screen  : " + screen.gameObject.name.ToString());


            if (screen.Modal && MainPanel != null) MainPanel.SetInteractable(true);

            if (screen.IsOpened && screen.gameObject.activeInHierarchy)
            {
                screen.UnsubscribeFromEvents();
                yield return StartCoroutine(screen.Hide());
                screen.gameObject.SetActive(false);
            }

            
        }

    }
}
