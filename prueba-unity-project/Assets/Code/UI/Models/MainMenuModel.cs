﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PruebaTecnica.UI
{
    public class MainMenuModel : Model
    {
        public string WelcomeText { get; private set; }

        public MainMenuModel(string welcomeText)
        {
            this.WelcomeText = welcomeText;
        }
    }
}
