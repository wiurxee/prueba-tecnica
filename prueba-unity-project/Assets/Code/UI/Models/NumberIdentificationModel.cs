﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PruebaTecnica.UI
{
    public class NumberIdentificationModel : Model
    {
        public int Number { get; private set; }
        public List<int> OtherNumbers { get; private set; }
        public NumberIdentificationModel(int min, int max, int numOfAnswers)
        {
            GenerateNewValues(min, max, numOfAnswers);
        }

        public void GenerateNewValues(int min, int max, int numOfAnswers)
        {
            Number = Random.Range(min, max + 1);

            OtherNumbers = new List<int>();

            int auxnumber;
            for (int i = 0; i < numOfAnswers - 1; i++)
            {
                do
                {
                    auxnumber = Random.Range(min, max + 1);
                }
                while (OtherNumbers.Contains(auxnumber) || auxnumber == Number);

                OtherNumbers.Add(auxnumber);
            }

        }
    }
}