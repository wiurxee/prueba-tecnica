﻿using System.Collections;
using UnityEngine;

namespace PruebaTecnica.UI
{
    public abstract class ViewBase : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract IEnumerator Show();
        public abstract IEnumerator Hide();
        public abstract void Destroy();

        public abstract void SetInteractable(bool value);
    }
}
