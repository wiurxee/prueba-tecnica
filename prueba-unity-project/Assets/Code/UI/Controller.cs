﻿using System.Collections;
using UnityEngine;

namespace PruebaTecnica.UI
{
    
    public abstract class Controller<TView, TModel> : ControllerBase where TView : View where TModel : Model
    {

        private bool Initialized;
        public virtual TView MyView { get; set; }
        public virtual TModel MyModel { get; set; }

        public virtual void InitializeController(TModel model)
        {
            if (!Initialized)
            {
                MyModel = model;

                MyView = gameObject.GetComponent<TView>();
                MyView.Initialize();

                Initialized = true;

                gameObject.SetActive(false);
            }
        }

        public void UpdateModel(TModel model)
        {
            MyModel = model;
        }
        public override void Destroy()
        {
            MyView.Destroy();
        }

        public override void SetInteractable(bool value)
        {
            MyView.SetInteractable(value);
        }

        public override IEnumerator Show(bool modal)
        {
            Modal = modal;
            IsOpened = true;
            yield return StartCoroutine(MyView.Show());            
        }

        public override IEnumerator Hide()
        {
            yield return StartCoroutine(MyView.Hide());
            IsOpened = false;
        }
    }
}
