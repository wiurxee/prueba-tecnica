﻿using PruebaTecnica.Game;
using System.Collections;
using UnityEngine;

namespace PruebaTecnica.UI
{
    [RequireComponent(typeof(NumberIdentificationStatementView))]
    public class NumberIdentificationStatementController : Controller<NumberIdentificationStatementView,NumberIdentificationModel>
    {
        public override void InitializeController(NumberIdentificationModel model)
        {
            base.InitializeController(model);

            MyView.SetTittle("IDENTIFICA EL NUMERO:");
            
        }

        public override IEnumerator Show(bool modal)
        {
            MyView.SetNumber(MyModel.Number);

            yield return StartCoroutine(base.Show(modal));

            yield return StartCoroutine(CloseAfterSeconds(2f));
        }

        public IEnumerator CloseAfterSeconds(float time)
        {
            yield return new WaitForSeconds(time);

            App.MyGameManager.ProcessComand(Command.AskAnswer);
        }

        public override void SubscribeToEvents()
        {
        }

        public override void UnsubscribeFromEvents()
        {

        }
    }
}