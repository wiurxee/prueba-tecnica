﻿using PruebaTecnica.Game;
using PruebaTecnica.ListExtensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PruebaTecnica.UI
{
    [RequireComponent(typeof(NumberIdentificationAnswersView))]
    public class NumberIdentificationAnswersController : Controller<NumberIdentificationAnswersView, NumberIdentificationModel>
    {
        private int m_Failures;

        public override void InitializeController(NumberIdentificationModel model)
        {
            base.InitializeController(model);

            MyView.SetTittle("Escoge la respuesta correcta:");
            
        }

        public override IEnumerator Show(bool modal)
        {
            List<int> answers = MyModel.OtherNumbers;
            answers.Add(MyModel.Number);

            answers.Shuffle();

            MyView.SetAnswers(answers);
            m_Failures = 0;

            yield return StartCoroutine(base.Show(modal));            
        }
        public override void SubscribeToEvents()
        {
            MyView.SubscribeAnswersClicks(OnAnswerClicked);
        }

        public override void UnsubscribeFromEvents()
        {
            MyView.UnsubscribeAnswersClicks(OnAnswerClicked);
        }

        private void OnAnswerClicked(int value)
        {
            StartCoroutine(CheckSolutionClicked(value));
        }

        private IEnumerator CheckSolutionClicked(int value)
        {

            MyView.SetInteractable(false);
            if (value == MyModel.Number)
            {                
                yield return StartCoroutine(MyView.ChangeColorOverTime(value, Color.green, 2f));

                App.MyGameManager.EndExercise(true);
            }
            else
            {
                m_Failures++;
                yield return StartCoroutine(MyView.ChangeColorOverTime(value, Color.red, 2f, true));

                if (m_Failures >= 2)
                {
                    yield return StartCoroutine(MyView.ChangeColorOverTime(MyModel.Number, Color.green, 2f, true));
                    App.MyGameManager.EndExercise(false);
                }
            }

            MyView.SetInteractable(true);
        }
    }
}