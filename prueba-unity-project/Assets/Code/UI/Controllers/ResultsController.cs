﻿using System.Collections;
using UnityEngine;
using PruebaTecnica.Game;

namespace PruebaTecnica.UI
{
    [RequireComponent(typeof(ResultsView))]
    public class ResultsController : Controller<ResultsView, EmptyModel>
    {
        public override void SubscribeToEvents()
        {
            
        }

        public override void UnsubscribeFromEvents()
        {
           
        }

        void Update()
        {
            MyView.SetSuccesses(App.MyGameManager.Successes);
            MyView.SetFailures(App.MyGameManager.Failures);
            MyView.SetDifficulty(App.MyGameManager.CurrentDifficult.ToString());
        }
    }
}
