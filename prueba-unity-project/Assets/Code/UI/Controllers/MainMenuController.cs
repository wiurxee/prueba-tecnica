﻿using System.Collections;
using UnityEngine;
using PruebaTecnica.Game;

namespace PruebaTecnica.UI
{
    [RequireComponent(typeof(MainMenuView))]
    public class MainMenuController : Controller<MainMenuView, MainMenuModel>
    {
        public override void InitializeController(MainMenuModel model)
        {
            base.InitializeController(model);

            MyView.SetWelcomeText(model.WelcomeText);            
        }
        public override void SubscribeToEvents()
        {
            MyView.SubscribeStartButton(OnStartButton);
        }
        public override void UnsubscribeFromEvents()
        {
            MyView.UnsubscribeStartButton(OnStartButton);
        }
        private void OnStartButton()
        {
            App.MyGameManager.ProcessComand(Command.BeginExercise);
        }

    }
}
