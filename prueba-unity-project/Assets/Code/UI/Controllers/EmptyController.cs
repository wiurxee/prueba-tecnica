﻿using System.Collections;
using UnityEngine;
using PruebaTecnica.Game;

namespace PruebaTecnica.UI
{
    [RequireComponent(typeof(EmptyView))]
    public class EmptyController : Controller<EmptyView, EmptyModel>
    {
        public override void SubscribeToEvents()
        {
        }

        public override void UnsubscribeFromEvents()
        {
        }
    }
}
