﻿
using System;
using System.Collections.Generic;

namespace PruebaTecnica.Game
{
    public enum ProcessState
    {
        Kickstart,
        MainMenu,
        NumberIdentificationStatement,
        NumberIdentificationAnswer,
        Resume
    }

    public enum Command
    {
        Kickstart,
        BeginExercise,
        AskAnswer,
        EndExercise
    }

    public class GameFiniteStateMachine
    {
        class StateTransition
        {
            readonly ProcessState CurrentState;
            readonly Command Command;

            public StateTransition(ProcessState currentState, Command command)
            {
                CurrentState = currentState;
                Command = command;
            }

            public override int GetHashCode()
            {
                return 17 + 31 * CurrentState.GetHashCode() + 31 * Command.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                StateTransition other = obj as StateTransition;
                return other != null && this.CurrentState == other.CurrentState && this.Command == other.Command;
            }
        }

        Dictionary<StateTransition, ProcessState> transitions;
        public ProcessState CurrentState { get; private set; }

        public GameFiniteStateMachine()
        {
            CurrentState = ProcessState.Kickstart;
            transitions = new Dictionary<StateTransition, ProcessState>
            {
                { new StateTransition(ProcessState.Kickstart, Command.Kickstart), ProcessState.MainMenu },
                { new StateTransition(ProcessState.MainMenu, Command.BeginExercise), ProcessState.NumberIdentificationStatement },
                { new StateTransition(ProcessState.NumberIdentificationStatement, Command.AskAnswer), ProcessState.NumberIdentificationAnswer},
                { new StateTransition(ProcessState.NumberIdentificationAnswer, Command.BeginExercise), ProcessState.NumberIdentificationStatement},
                { new StateTransition(ProcessState.NumberIdentificationStatement, Command.EndExercise), ProcessState.Resume},
                { new StateTransition(ProcessState.NumberIdentificationAnswer, Command.EndExercise), ProcessState.Resume}
            };
        }

        public ProcessState GetNext(Command command)
        {
            StateTransition transition = new StateTransition(CurrentState, command);
            ProcessState nextState;
            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("[Game Finite State Machine] Invalid transition: " + CurrentState + " -> " + command);
            return nextState;
        }

        public ProcessState MoveNext(Command command)
        {
            CurrentState = GetNext(command);
            return CurrentState;
        }

    }
}
