﻿using UnityEngine;
using PruebaTecnica.UI;
using System.Collections.Generic;
using PruebaTecnica.EnumExtensions;

namespace PruebaTecnica.Game
{
    public class GameManager
    {
        /// <summary>
        /// Differents Difficulties of the exercise
        /// </summary>
        public enum Difficulty
        { 
            Easier,
            Novice,
            Normal,
            Medium,
            Hard,
            Epic
        }

        /// <summary>
        /// struct for declaring the conditions of each difficulty
        /// </summary>
        public struct Level
        {
            public int MinNumber;
            public int MaxNumber;
            public int NumberAnswers;

            public Level(int min, int max, int numberAnswes)
            {
                this.MinNumber = min;
                this.MaxNumber = max;
                this.NumberAnswers = numberAnswes;
            }
        }

        /// <summary>
        /// Definition of the different Difficult levels
        /// </summary>
        private Dictionary<Difficulty, Level> m_DifficultyLevels = new Dictionary<Difficulty, Level>(){
            { Difficulty.Easier, new Level(-20,20,3)},
            { Difficulty.Novice, new Level(-50,50,3)},
            { Difficulty.Normal, new Level(-100,100,4)},
            { Difficulty.Medium, new Level(-1000,1000, 5)},
            { Difficulty.Hard, new Level(-99999,99999, 5)},
            { Difficulty.Epic, new Level(-999999, 999999, 6)}        
        };

        public int Successes;
        public int Failures;
        public Difficulty CurrentDifficult = Difficulty.Easier;

        private int m_SuccesesInARow;
        private int m_FailuresInARow;


        private NumberIdentificationModel m_NumberIdentificationModel;
        private MainMenuModel m_MainMenuModel;

        private GameFiniteStateMachine m_GameFiniteStateMachine;

        private ResultsController m_ResultsController;
        private MainMenuController m_MainMenuController;
        private NumberIdentificationStatementController m_NumberIdentificationStatementController;
        private NumberIdentificationAnswersController m_NumberIdentificationAnswersController;
        public GameManager()
        {
            m_GameFiniteStateMachine = new GameFiniteStateMachine();

            m_MainMenuModel = CreateMainMenuModel();
            m_NumberIdentificationModel = CreateNumberIdentificationModel();


            m_ResultsController = (ResultsController)UIManager.Instance.CreateScreen("results-panel");
            m_ResultsController.ClosingAllowed = false;
            m_ResultsController.InitializeController(new EmptyModel());

            m_MainMenuController = (MainMenuController) UIManager.Instance.CreateScreen("main-menu");
            m_MainMenuController.InitializeController(m_MainMenuModel);

            m_NumberIdentificationStatementController = (NumberIdentificationStatementController) UIManager.Instance.CreateScreen("number-identification-statement");
            m_NumberIdentificationStatementController.InitializeController(m_NumberIdentificationModel);

            m_NumberIdentificationAnswersController = (NumberIdentificationAnswersController) UIManager.Instance.CreateScreen("number-identification-answer");
            m_NumberIdentificationAnswersController.InitializeController(m_NumberIdentificationModel);


            //UIManager.Instance.CreateScreen("resume");

            ProcessComand(Command.Kickstart);
        }

        public void ProcessComand(Command command)
        {
            ProcessState current = m_GameFiniteStateMachine.CurrentState;

            ProcessState next = m_GameFiniteStateMachine.MoveNext(command);

            if (current != next)
            {
                switch (next)
                {
                    case ProcessState.MainMenu:
                        UIManager.Instance.ShowScreen(m_MainMenuController, UIManager.Instance.MainPanel);
                        break;
                    case ProcessState.NumberIdentificationStatement:
                        if (!m_ResultsController.IsOpened) UIManager.Instance.ShowScreen(m_ResultsController, UIManager.Instance.AlwaysOnTop, closeOthers:false);

                        m_NumberIdentificationStatementController.UpdateModel(m_NumberIdentificationModel);
                        UIManager.Instance.ShowScreen(m_NumberIdentificationStatementController, UIManager.Instance.MainPanel);
                        break;
                    case ProcessState.NumberIdentificationAnswer:
                        m_NumberIdentificationAnswersController.UpdateModel(m_NumberIdentificationModel);
                        UIManager.Instance.ShowScreen(m_NumberIdentificationAnswersController, UIManager.Instance.MainPanel);
                        break;
                    case ProcessState.Resume:
                        break;
                }
            }
        }

        private MainMenuModel CreateMainMenuModel()
        {
            return new MainMenuModel("Bienvenido al ejercicio de identificacion de números");
        }
        private NumberIdentificationModel CreateNumberIdentificationModel()
        {
            return  new NumberIdentificationModel(m_DifficultyLevels[CurrentDifficult].MinNumber
                                                                         , m_DifficultyLevels[CurrentDifficult].MaxNumber
                                                                         , m_DifficultyLevels[CurrentDifficult].NumberAnswers);
        }
        public void EndExercise(bool completed)
        {
            if (completed)
            {
                Successes++;
                m_SuccesesInARow++;
                m_FailuresInARow = 0;

                if (m_SuccesesInARow >= 3)
                {
                    CurrentDifficult = CurrentDifficult.Next();
                }
            }
            else
            {
                Failures++;
                m_FailuresInARow++;
                m_SuccesesInARow = 0;

                if (m_FailuresInARow >= 2)
                {
                    CurrentDifficult = CurrentDifficult.Previous();
                }
            }

            m_NumberIdentificationModel = CreateNumberIdentificationModel();

            ProcessComand(Command.BeginExercise);
        }
    }
}
