﻿using UnityEditor;

namespace PruebaTecnica.UI
{
    [CustomEditor(typeof(ExtendedText))]
    public class ExtendedTextEditor : UnityEditor.UI.TextEditor
    {
        SerializedProperty Format;
        SerializedProperty BackGroundImage;
        protected override void OnEnable()
        {
            base.OnEnable();
            Format = serializedObject.FindProperty("Format");
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();        

            EditorGUILayout.PropertyField(Format);

            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();

        }
    }
}
