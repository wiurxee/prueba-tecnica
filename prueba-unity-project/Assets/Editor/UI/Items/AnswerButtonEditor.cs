﻿using UnityEditor;

namespace PruebaTecnica.UI
{
    [CustomEditor(typeof(AnswerButton))]
    public class AnswerButtonEditor:UnityEditor.UI.ButtonEditor
    {
        SerializedProperty InnerText;
        SerializedProperty BackGroundImage;
        protected override void OnEnable()
        {
            base.OnEnable();
            InnerText = serializedObject.FindProperty("InnerText");
            BackGroundImage = serializedObject.FindProperty("BackGroundImage");
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            AnswerButton answerButton = (AnswerButton)target;

            answerButton.AnswerNumber = EditorGUILayout.IntField("Answer Number", answerButton.AnswerNumber);

            EditorGUILayout.PropertyField(InnerText);
            EditorGUILayout.PropertyField(BackGroundImage);


            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
            
        }
    }
}
